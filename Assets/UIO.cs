using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIO : MonoBehaviour

{
    public GameObject light;
    public GameObject player;
    public bool TorchOn;
    private float MaxDelay = 2f;
    private float Delay;
    public float sec = 3f;
    public float countdownValue;



    public void O1()
    {
        TorchOn = true;
        //light.SetActive(true);

    }


    public void O2()
    {

    }
    // Start is called before the first frame update
    void Start()
    {

        light.SetActive(false);
    }
    private IEnumerator ActivationRoutine()
    {
        if (TorchOn == true)
        {
            yield return new WaitForSeconds(1);
            light.SetActive(true);
            TorchOn = false;
        }
        else  { 
       
        yield return new WaitForSeconds(1);
        light.SetActive(false);
            }
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine(ActivationRoutine());



    }



}
