using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using static UnityEngine.GraphicsBuffer;

public class SHootingtargets : MonoBehaviour
{


    public GameObject Player;
    public GameObject projectile;

    public GameObject SphereSpawn;

    [SerializeField]
    private ParticleSystem expPar;

    // Start is called before the first frame update
    void Start()
    {
        myTransform = transform;
        shooted = false;
        //StartCoroutine(SimulateProjectile());
    }

    void Spawn()
    {
        Vector3 rand = Random.onUnitSphere * 540;
        Debug.Log(rand);
        Instantiate(this, rand, transform.rotation);

    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.T))
        {
            shooted = false;
            Projectile = Instantiate(projectile, myTransform.position + Vector3.up * 1, Quaternion.identity).transform;
            StartCoroutine(SimulateProjectile());

            Spawn();

        }
        Collider[] colliders = Physics.OverlapSphere(transform.position, 15f);

        foreach (Collider hit in colliders)
        {
            if (hit == Player.GetComponent<CapsuleCollider>() && !shooted)
            {
                shooted = true;
                Debug.Log("Player is closeee");
                Projectile = Instantiate(projectile, myTransform.position + Vector3.up * 1, Quaternion.identity).transform;
                StartCoroutine(SimulateProjectile());

            }

        }
        //ShootAtTarget(Player.transform.position);

    }

    private float radius = 3f;
    private float power = 10f;
    public float mass;


    void Explode(Transform obj)
    {
        //    Rigidbody rb = obj.GetComponent<Rigidbody>();

        //if (rb != null)
        //    rb.AddExplosionForce(power, obj.transform.position, radius, 3.0F);

        //Vector3 explosionPos = transform.position;
        Debug.Log("pos" + positionToExp);
        Vector3 explosionPos = positionToExp;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);

        Instantiate(expPar, explosionPos, Quaternion.identity);

        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            //rb.mass = mass;

            if (rb != null)
            {
                rb.mass = 0.010f;
                rb.AddExplosionForce(power, explosionPos, radius, 3.0F);
            }
        }
    }

    public Transform Target;
    public float firingAngle = 45.0f;
    public float gravity = 9.8f;

    public Transform Projectile;
    private Transform myTransform;
    private Vector3 positionToExp;
    private bool shooted;

    IEnumerator SimulateProjectile()
    {
        // Short delay added before Projectile is thrown
        yield return new WaitForSeconds(0.5f);

        // Move projectile to the position of throwing object + add some offset if needed.
        projectile.transform.position = myTransform.position + new Vector3(0, 0.0f, 0);

        // Calculate distance to target
        float target_Distance = Vector3.Distance(Projectile.position, Target.position);

        // Calculate the velocity needed to throw the object to the target at specified angle.
        float projectile_Velocity = target_Distance / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad) / gravity);

        // Extract the X  Y componenent of the velocity
        float Vx = Mathf.Sqrt(projectile_Velocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad);
        float Vy = Mathf.Sqrt(projectile_Velocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad);

        // Calculate flight time.
        float flightDuration = target_Distance / Vx;

        // Rotate projectile to face the target.
        Projectile.rotation = Quaternion.LookRotation(Target.position - Projectile.position);

        float elapse_time = 0;

        while (elapse_time < flightDuration)
        {
            Projectile.Translate(0, (Vy - (gravity * elapse_time)) * Time.deltaTime, Vx * Time.deltaTime);

            elapse_time += Time.deltaTime;

            yield return null;
        }

        positionToExp = Projectile.transform.position;
        Explode(Projectile);
    }
}
