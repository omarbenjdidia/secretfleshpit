using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ThrowWrench : MonoBehaviour
{
    Rigidbody rb;

    public float speed = 0.02f;

    private Transform target;

    private float moveSpeedFollow = 1f;
    public float rotateSpeed = 200f;



    public float MoveSpeed5 = 1f;
    public NavMeshAgent navMeshAgent;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        
        target = GameObject.Find("Sphere").transform;
        navMeshAgent= GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        navMeshAgent.SetDestination(target.position);
        Vector3 direction = (Vector3)target.position - rb.position;

        direction.Normalize();

        //Vector3 rotateAmount = Vector3.Cross(direction, transform.forward);

        //rb.angularVelocity = -rotateAmount * rotateSpeed;

        //rb.velocity = transform.forward * speed;

    }


}

