using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PullMec : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    
    }

    public float explosionRadius = 2.0f;

    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        //Gizmos.DrawSphere(transform.position, 0.1f);
        Gizmos.DrawWireSphere(transform.position, explosionRadius);

    }


    // Update is called once per frame
    void Update()
    {
        
    }
    public float pullRadius = 2;
    public float pullForce = 1;

    public void FixedUpdate()
    {
        foreach (Collider collider in Physics.OverlapSphere(transform.position, pullRadius)) {
       
            if (collider.gameObject.tag == "item")
            {
                Debug.Log("fama");

            // calculate direction from target to me
            Vector3 forceDirection = transform.position - collider.transform.position;

            // apply force on target towards me
            collider.GetComponent<Rigidbody>().AddForce(forceDirection.normalized );
            }
        }
    }

}
