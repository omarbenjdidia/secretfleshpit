using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class testt : MonoBehaviour
{
    public Image image;
    public float cooldown = 5f;

    public bool isOnCooldown = false;

    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
    }

    public void ChangeFill()
    {
            if(isOnCooldown== false)
        {
            isOnCooldown = true;
            image.fillAmount = 1;
        }

    }


    // Update is called once per frame
    void Update()
    {

        if (isOnCooldown)
        {
            image.fillAmount -= 1 / cooldown * Time.deltaTime;
            if (image.fillAmount <= 0)
            {
                image.fillAmount = 1;
                isOnCooldown = false;
            }

        }
    }
}
