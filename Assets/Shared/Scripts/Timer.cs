using TMPro;
using UnityEditor.Experimental.GraphView;
using UnityEditorInternal;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public TextMeshProUGUI timertext;
    private float starttime;
    public Infectedzone Infectedzone;
    public bool Timeup;

    // Start is called before the first frame update
    void Start()
    {
        timertext = GetComponent<TextMeshProUGUI>();
        starttime = Time.time;
        Infectedzone= FindObjectOfType<Infectedzone>();
        starttime = 180f;
    }

    // Update is called once per frame
    void Update()
    {
        if(Infectedzone.win)
        {
            timertext.color = Color.green;
            return;
        }
        float t = starttime - Time.time;
        string min = ((int)t / 60).ToString();
        string sec = (t % 60).ToString("f0");
        timertext.text = min + ":" + sec;
    }
}
