using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Infectedzone : MonoBehaviour
{
    public GameObject Cure;
    public GameObject CureVFX;
    public GameObject GasVFX;
    private GameObject _GasVFX;
    public bool win;

    void Start()
    {
        _GasVFX= Instantiate(GasVFX, transform.position+ new Vector3(0f, -0.5f, 0f) , transform.rotation);
    }

    void Update()
    {
        MapCuring();
    }

    public void MapCuring()
    {

    Collider[] colliders = Physics.OverlapSphere(transform.position, 5f);
                foreach (Collider hit in colliders)
                {
                    Rigidbody rb = hit.GetComponent<Rigidbody>();
                    if (rb != null && rb == GameObject.Find("ThirdPersonController1").GetComponent<Rigidbody>())
                    {
                           if(GameObject.Find("Crystal Heart Variant(Clone)")== null)
                           { 
                                    win = true;
                                    GameObject cure= Instantiate(Cure, transform.position+ new Vector3(0f, 1.5f, 0f), transform.rotation);
                                    GameObject curevfx = Instantiate(CureVFX, transform.position + new Vector3(0f, 1.5f, 0f), transform.rotation);
                                    Destroy(cure, 3f);
                                    Destroy(_GasVFX, 1f);
                                    Destroy(curevfx, 3f);
                                    Destroy(gameObject, 1.5f);
                           }
          
                    }
                }
    }

}
    