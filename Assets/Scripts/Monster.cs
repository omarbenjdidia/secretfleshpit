using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Monster : MonoBehaviour
{
    private NavMeshAgent Monstenavigation;
    public GameObject Character;

    // Start is called before the first frame update
    void Start()
    {
        Monstenavigation= GetComponent<NavMeshAgent>(); 
    }

    // Update is called once per frame
    void Update()
    {
        Monstenavigation.SetDestination(Character.transform.position);
    }
}
