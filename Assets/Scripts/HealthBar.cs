using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private float health;
    public Neutrophil neutrophil;
    public Slider slider;
    // Start is called before the first frame update
    void Start()
    {
        health = 100f;
        neutrophil = FindObjectOfType<Neutrophil>();
        slider = GetComponent<Slider>();    
    }

    // Update is called once per frame
    void Update()
    {
        
    }

     void FixedUpdate()
    {
        Damage();
    }

    public void Damage()
    {
        if (neutrophil.IsDamaged == true)
        {
            health -= 10f;
            neutrophil.IsDamaged = false;
            slider.value= health;
        }

        if (health <=0f)
        {
            slider.value = health;
            neutrophil.NeutroAnim.SetBool("death", true);
            Destroy(GameObject.FindGameObjectWithTag("Character").transform.GetChild(1).gameObject, 2.0f);

        }

    }
}
