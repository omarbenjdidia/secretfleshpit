using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Unity.Android.Types;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;
using UnityEngine.UIElements;
using static UnityEngine.GraphicsBuffer;

public class UI : MonoBehaviour
{
    public Transform myPosition;
    public Transform Bombposition;
    float range = 30f;
    public GameObject Bomb;
    private Rigidbody rigidbomb;
    private BoxCollider colliderbomb;
    [SerializeField] private Transform spawnPoint;
    public float power = 300F;
    public GameObject VFX2;
    public GameObject VFX1;
    public GameObject bacteriaSpawn;
    private Vector3 spawnPosition;

    public GameObject _bac1;
    public GameObject _bac2;
    public GameObject _bac3;

    public GameObject Target;

    private float MaxDelay = 5f;
    private float Delay;
    private bool Isclicked;
    public float newhealth;

    private int EnemeyCount;
    private int Xpos;
    private int Zpos;


    void Start()
    {
        //BacteriaInstance();
        StartCoroutine(EnemySpawn());
    }

    void Update()
    {
        FindClosestEnemy();
        CollectCollidersInRangeAndExplode();
        WaitFunction();
    }
    public void Skill1()
    {
        Collider[] colliders = Physics.OverlapSphere(myPosition.position, range);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if (rb != null && (rb == _bac1.GetComponent<Rigidbody>() || rb == _bac2.GetComponent<Rigidbody>() || rb == _bac3.GetComponent<Rigidbody>()))
            {
                GameObject vfx= Instantiate(VFX1, myPosition.position, myPosition.rotation);
                Destroy(vfx, 1.0f);
                rb.gameObject.GetComponent<NavMeshAgent>().speed = 0.5f;
                newhealth = rb.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.GetComponent<HealthBarMonster>().health -= 10f;
                rb.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.GetComponent<HealthBarMonster>().slider.value = newhealth;
                Isclicked = true;
            }
        }
    }

    public void Skill2()
    {
        if (GameObject.Find("Bomb(Clone)")== null)
        {
            GameObject _bomb= Instantiate(Bomb, spawnPoint.position+new Vector3(0, 25f, 0), spawnPoint.rotation);
            _bomb.transform.localScale = new Vector3(1f, 1f, 1f);
            rigidbomb = GameObject.Find("Bomb(Clone)").AddComponent<Rigidbody>();
            colliderbomb = GameObject.Find("Bomb(Clone)").AddComponent<BoxCollider>();
            Bombposition = GameObject.Find("Bomb(Clone)").transform;
            Bombposition.position = Vector3.MoveTowards(GameObject.Find("Bomb(Clone)").transform.position, Target.transform.position+ new Vector3(0, 8f, 0), 50f);
        }
    }


    public void CollectCollidersInRangeAndExplode()
    {
        if (rigidbomb != null)
        {
            if (rigidbomb.IsSleeping())
            {
                Collider[] colliders = Physics.OverlapSphere(Bombposition.position, range);
                foreach (Collider hit in colliders)
                {
                    Rigidbody rb = hit.GetComponent<Rigidbody>();
                    if (rb != null && (rb== _bac1.GetComponent<Rigidbody>() || rb== _bac2.GetComponent<Rigidbody>() || rb== _bac3.GetComponent<Rigidbody>()))
                    {
                        rb.AddExplosionForce(power, Bombposition.position, range, 3.0F);
                        newhealth= rb.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.GetComponent<HealthBarMonster>().health-= 40f;
                        rb.transform.GetChild(0).transform.GetChild(0).GetComponent<HealthBarMonster>().slider.value = newhealth;
                    }
                }
                Instantiate(VFX2, Bombposition.position, Bombposition.rotation);
                Destroy(GameObject.Find("Bomb(Clone)"));
                Destroy(GameObject.Find("BigExplosion(Clone)"), 2.0f);
            }
        }
    }

    public void BacteriaInstance()
    {
        myPosition = GameObject.FindGameObjectWithTag("Character").GetComponent<Transform>();
        spawnPosition = new Vector3(myPosition.position.x, myPosition.position.y, myPosition.position.z + 25f);
        _bac1 = Instantiate(bacteriaSpawn, spawnPosition+ new Vector3(0, 0, -60f), spawnPoint.rotation);
        _bac2 = Instantiate(bacteriaSpawn, spawnPosition+ new Vector3(0,0,-70f), spawnPoint.rotation);
        _bac3 = Instantiate(bacteriaSpawn, spawnPosition+ new Vector3(0, 0, -50f), spawnPoint.rotation);
    }

    IEnumerator EnemySpawn()
    {
        while(EnemeyCount< 1)
        {
            Xpos = Random.Range(1, 7);
            Zpos = Random.Range(1, 10);
            _bac1 = Instantiate(bacteriaSpawn, new Vector3(Xpos, spawnPosition.y, Zpos), spawnPoint.rotation);
            _bac2 = Instantiate(bacteriaSpawn, new Vector3(Xpos, spawnPosition.y, Zpos), spawnPoint.rotation);
            _bac3 = Instantiate(bacteriaSpawn, new Vector3(Xpos, spawnPosition.y, Zpos), spawnPoint.rotation);
            yield return new WaitForSeconds(0.1f);
            EnemeyCount++;
        }
    }
    public void FindClosestEnemy()
    {
            float Distancetoclosestbacteria = Mathf.Infinity;
            GameObject Closestbacteria = null;
            GameObject[] Allbacteria = { _bac1, _bac2, _bac3 };
         
            foreach (GameObject Currentbacteria in Allbacteria)
            {
            if(Currentbacteria!= null)
            {
              float Distancetobacteria = (Currentbacteria.transform.position - GameObject.Find("NeutrophilFinal").transform.position).sqrMagnitude;
              if (Distancetobacteria < Distancetoclosestbacteria)
                {
                  Distancetoclosestbacteria = Distancetobacteria;
                  Closestbacteria = Currentbacteria;
                  Target = Closestbacteria;

                }
            }
            }
    }

    public void WaitFunction()
    {
        if (Isclicked == true)
        {
            Delay += Time.deltaTime;
            if (Delay > MaxDelay)
            {
                if(_bac1!= null)
                    _bac1.GetComponent<NavMeshAgent>().speed = 1f;
                if (_bac2 != null)
                    _bac2.GetComponent<NavMeshAgent>().speed = 1f;
                if (_bac3 != null)
                    _bac3.GetComponent<NavMeshAgent>().speed = 1f;
                Isclicked = false;
                
            }
        }
    }
        
}
