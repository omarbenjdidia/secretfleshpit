using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;
using static UnityEngine.GraphicsBuffer;

public class UIY : MonoBehaviour
{
    public Transform myPosition;
    public float range= 5f;
    private GameObject Enemy;
    public GameObject Potion;
    public GameObject Bac;
    public bool skill2active;
    public GameObject VFX1;
    public GameObject VFX2;
    public GameObject VFX3;
    private GameObject obj;
    private GameObject obj1;


    public GameObject CharacterToHeal; 
    public float newhealthteammate;
    public float newhealthtmonster;
    public float newhealthtmonster1;
    public float newhealthcharacter;
    public float force= 10f;
    public UI ui;
    private GameObject _Enemy;

    // Start is called before the first frame update
    void Start()
    {
        _Enemy= Enemy = Instantiate(Bac, myPosition.position + new Vector3(40f, 0, 20f), myPosition.rotation);
        ui= FindObjectOfType<UI>();
    }

    // Update is called once per frame
    void Update()
    {
        if(_Enemy != null)
            _Enemy.GetComponent<NavMeshAgent>().SetDestination(myPosition.position);
        ThrowPotion();
    }

    public void Skill1()
    {
        Collider[] colliders = Physics.OverlapSphere(myPosition.position, range);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if (rb != null && (rb == CharacterToHeal.GetComponent<Rigidbody>()))
            {
                newhealthcharacter = gameObject.transform.GetChild(0).gameObject.GetComponent<MyhealthBar>().health;
                newhealthteammate = rb.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.GetComponent<HealthBarTeammate>().health;
                if(newhealthcharacter<100)
                newhealthcharacter += 50f;
                if (newhealthteammate<100)
                newhealthteammate += 50f;
                rb.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.GetComponent<HealthBarTeammate>().slider.value = newhealthteammate;
                gameObject.transform.GetChild(0).gameObject.GetComponent<MyhealthBar>().slider.value = newhealthcharacter;
            }
        }
        GameObject vfx = Instantiate(VFX1, myPosition.position, myPosition.rotation);
        Destroy(vfx, 3.0f);
    }

    public void Skill2()
    {
        if(obj== null)
        {
        obj= Instantiate(VFX2, myPosition.position , myPosition.rotation);
        Destroy(obj, 2f);
        }

    }

    public void ThrowPotion()
    {

        if(obj!= null)
        {
            obj.transform.position = myPosition.position;
            Collider[] colliders = Physics.OverlapSphere(myPosition.position, range);
            foreach (Collider hit in colliders)
            {

                Rigidbody rb = hit.GetComponent<Rigidbody>();
                if (rb != null && rb == _Enemy.GetComponent<Rigidbody>() && obj1== null) 
                {
                    newhealthtmonster1 = rb.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.GetComponent<HealthBarMonster1>().health -= 3f;
                    rb.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.GetComponent<HealthBarMonster1>().slider.value = newhealthtmonster1;
                    obj1 = Instantiate(VFX3, rb.gameObject.transform.position, rb.gameObject.transform.rotation);
                    Destroy(obj1, 0.2f);
                }

                if ((rb != null && (rb == ui._bac2.GetComponent<Rigidbody>() || rb == ui._bac2.GetComponent<Rigidbody>() || rb == ui._bac3.GetComponent<Rigidbody>())) && obj1 == null)
                {
                    newhealthtmonster = rb.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.GetComponent<HealthBarMonster>().health -= 3f;
                    rb.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.GetComponent<HealthBarMonster>().slider.value = newhealthtmonster;

                    obj1 = Instantiate(VFX3, rb.gameObject.transform.position, rb.gameObject.transform.rotation);
                    Destroy(obj1, 0.2f);
                }
            }

        }
    }



}



