using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterYounes : MonoBehaviour
{
    public GameObject VFX;
    public float newhealthcharacter;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.name== "ThirdPersonController1")
        {
            GameObject.Find("ThirdPersonController1").GetComponent<Rigidbody>().AddExplosionForce(100f, transform.position, 10f, 3.0F);
            GameObject vfx= Instantiate(VFX, transform.position, transform.rotation);
            Destroy(gameObject);
            Destroy(vfx, 1.5f);
            newhealthcharacter = GameObject.Find("MobileSingleStickControl1").transform.GetChild(0).gameObject.GetComponent<MyhealthBar>().health -= 50f;
            GameObject.Find("MobileSingleStickControl1").transform.GetChild(0).gameObject.GetComponent<MyhealthBar>().slider.value = newhealthcharacter;
        }
    }
}
