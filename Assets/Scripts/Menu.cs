﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{

    public GameObject MainCam;
    public GameObject MenuCam;

    public GameObject MainCanvas;
    public GameObject MenuCanvas;
    public void Pause()
    {
        MainCam.SetActive(false);
        MenuCam.SetActive(true);
        MainCanvas.SetActive(false);
        MenuCanvas.SetActive(true);
    }


    public void Resume()
    {
        MainCam.SetActive(true);
        MenuCam.SetActive(false);
        MainCanvas.SetActive(true);
        MenuCanvas.SetActive(false);
    }

}
