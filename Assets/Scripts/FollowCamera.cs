using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    //camera related variables
    public Transform target;
    public float smoothSpeed = 0.125f;
    public Vector3 locationOffset;
    public Vector3 rotationOffset;

    //// Start is called before the first frame update
    //[SerializeField]
    //Transform Player;
    //[SerializeField]
    //float offset;
    //void Start()
    //{

    //}

    //// Update is called once per frame
    //void FixedUpdate()
    //{
    //    transform.position = new Vector3(Player.position.x, 12f, Player.position.z + offset);
    //}
    public void FixedUpdate()
    {
        Camera3D();
    }
    public void Camera3D()
    {
        Vector3 desiredPosition = target.position + target.rotation * locationOffset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        transform.position = smoothedPosition;
        Quaternion desiredrotation = target.rotation * Quaternion.Euler(rotationOffset);
        Quaternion smoothedrotation = Quaternion.Lerp(transform.rotation, desiredrotation, smoothSpeed);
        transform.rotation = smoothedrotation;
    }
}
