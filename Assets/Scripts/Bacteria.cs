using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;

public class Bacteria : MonoBehaviour
{
    Transform Character;
    private NavMeshAgent bacteria;
    [SerializeField] private Transform spawnPoint;
    public GameObject Spit;
    private NavMeshAgent navspit;
    private GameObject _spit;
    public GameObject Healthbar;
    public GameObject _healthbar;

    private GameObject Target;
    //private HealthBarMonster health;


    void Start()
    {
        bacteria = GetComponent<NavMeshAgent>();
        Character = GameObject.FindGameObjectWithTag("Character").GetComponent<Transform>();
        
    }

    // Update is called once per frame
    void Update()
    {
        FindClosestEnemy();
        bacteria.SetDestination(Target.transform.position);
        SpitInstance();

    }

    public void SpitInstance()
    {
        if (_spit == null)
            _spit = Instantiate(Spit, spawnPoint.position, spawnPoint.rotation);
        navspit = _spit.GetComponent<NavMeshAgent>();
        navspit.SetDestination(Character.position);
        Destroy(_spit, 4f);
    }

    public void HealthBarInstance()
    {
        _healthbar = Instantiate(Healthbar, new Vector3(spawnPoint.position.x, spawnPoint.position.y + 1f, spawnPoint.position.z - 0.5f), spawnPoint.rotation);
        _healthbar.transform.SetParent(gameObject.transform);
    }

    public void FindClosestEnemy()
    {
        float Distancetoclosestcharacter = Mathf.Infinity;
        GameObject Closestcharacter = null;
        GameObject[] Characters = { Character.gameObject /* put all characters here*/ };

        foreach (GameObject Currentcharacter in Characters)
        {
            if (Currentcharacter != null)
            {
                float Distancetocharacter = (Currentcharacter.transform.position - Character.position).sqrMagnitude;
                if (Distancetocharacter < Distancetoclosestcharacter)
                {
                    Distancetoclosestcharacter = Distancetocharacter;
                    Closestcharacter = Currentcharacter;
                    Target = Closestcharacter;

                }
            }
        }
    }
}
